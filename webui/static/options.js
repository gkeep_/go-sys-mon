export class Colors {
    constructor() {
        this.Bg     = getComputedStyle(document.body).getPropertyValue('--color-background');
        this.Fg     = getComputedStyle(document.body).getPropertyValue('--color-foreground');
        this.FgAlt  = getComputedStyle(document.body).getPropertyValue('--color-foreground-alt');
        this.Grid   = getComputedStyle(document.body).getPropertyValue('--color-grid');
        this.Blue   = getComputedStyle(document.body).getPropertyValue('--color-blue');
        this.Orange = getComputedStyle(document.body).getPropertyValue('--color-orange');
        this.Purple = getComputedStyle(document.body).getPropertyValue('--color-purple');
        this.Green  = getComputedStyle(document.body).getPropertyValue('--color-green');
        this.Red    = getComputedStyle(document.body).getPropertyValue('--color-red');
    
        this.ChartColors = [
            "#7B68EE",
            "#FFFF00",
            "#00FFFF",
            "#DC143C",
            "#FF00FF",
            "#32CD32",
            "#4169E1",
            "#7FFFD4",
            "#0000FF",
            "#808000",
            "#008080",
            "#4682B4",
            "#8A2BE2",
            "#FF7F50",
            "#00FF7F"
        ]
    }

    getColorList(len) {
        var outList = this.ChartColors

        while (outList.length < len) {
            outList.push(this.Fg)
        }

        return outList
    }
}

const colors = new Colors();

export class Options {
    constructor() {
        this.CPU = {
            chart: {
                height: 350,
                type: "line",
                stacked: false,
                animations: {
                    enabled: false,
                    easing: "linear",
                    dynamicAnimation: {
                        speed: 15
                    }
                },
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                },
                fontFamily: "Noto Sans, Arial, sans-serif",
            },
            colors: colors.getColorList(4),
        
            dataLabels: {
                enabled: false
            },
        
            stroke: {
                curve: 'smooth',
                width: 3,
            },
            grid: {
                show: true,
                borderColor: colors.Grid, 
                xaxis: {
                    lines: {
                        show: false
                    }
                },
            },
            markers: {
                size: 0,
                hover: {
                    size: 0
                }
            },
        
            xaxis: {
                range: 30,
                tickPlacement: "on",
                labels: {
                    hideOverlappingLabels: true,
                },
                crosshairs: {
                    show: false
                },
                axisBorder: {
                    show: true,
                    color: colors.Fg,
                },
                axisTicks: {
                    show: true,
                    borderType: 'solid',
                    color: colors.Fg,
                },
                labels: {
                    show: true,
                    formatter: function(val) {
                        return (val+"s");
                    },
                },
            },
            yaxis: {
                max: 100,
                min: 0,
                axisBorder: {
                    show: true,
                    color: colors.Grid,
                },
                labels: {
                    show: true,
                    formatter: function(val) {
                        return (val.toFixed(0)+"%");
                    },
                },
            },
        
            legend: {
                show: true,
                position: "right",
                fontFamily: "Noto Sans, Arial, sans-serif",
                fontSize: "14px",
                labels: {
                    colors: colors.Fg,
                },
            },
            tooltip: {
                enabled: false,
            },
            noData: {
                text: 'Loading...'
            },
        
            fill: {
                type: "solid"
            },
        
            series: [],
            title: {
                text: 'CPU Usage',
                align: "left",
                style: {
                    fontFamily: "Noto Sans, Arial, sans-serif",
                    fontSize: "16px",
                    fontWeight: 700,
                }
            },
        }

        this.RAM = {
            chart: {
                height: 300,
                type: 'radialBar',
            },
            title: {
                text: 'Memory Usage',
                align: 'left',
                style: {
                    fontSize: "16px",
                    fontFamily: "Noto Sans, Arial, sans-serif",
                    fontWeight: 700,
                }
            },
        
            series: [],
            labels: ['Memory', 'Swap'],
            legend: {
                show: true,
                position: "right",
                horizontalAlign: 'left',            
                fontFamily: "Noto Sans, Arial, sans-serif",
                fontSize: "14px",
            },
        
            plotOptions: {
                radialBar: {
                    inverseOrder: false,
                    hollow: {
                        margin: 0,
                        size: "20%",
                        background: "transparent"
                    },
                    track: {
                        show: false,
                    }
                },
            },
        
            fill: {
                type: "solid"
            },
        
            noData: {
                text: 'Loading...'
            }
        }

        this.Network = {
            chart: {
                id: 'network-chart',
                height: 300,
                type: "line",
                stacked: false,
                animations: {
                    enabled: true,
                    easing: "linear",
                    dynamicAnimation: {
                        speed: 50
                    }
                },
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                },
                brush: {
                    enabled: true,
                    autoScaleYaxis: true,
                    target: 'network-chart',
                },
                fontFamily: "Noto Sans, Arial, sans-serif",
            },
        
            dataLabels: {
                enabled: false
            },
        
            stroke: {
                curve: 'smooth',
                width: 3,
            },
            grid: {
                show: true,
                borderColor: colors.Grid, 
                xaxis: {
                    lines: {
                        show: false
                    }
                },
            },
            markers: {
                size: 0,
                hover: {
                    size: 0
                }
            },
        
            xaxis: {
                fontFamily: "Noto Sans, Arial, sans-serif",
                range: 10,
                tickPlacement: "on",
                labels: {
                    hideOverlappingLabels: false,
                },
                crosshairs: {
                    show: false
                },
                axisBorder: {
                    show: true,
                    color: colors.Fg,
                },
                axisTicks: {
                    show: true,
                    borderType: 'solid',
                    color: colors.Grid,
                },
                labels: {
                    show: true,
                    formatter: function(val) {
                        return (val+"s");
                    },
                },
            },
            yaxis: {
                fontFamily: "Noto Sans, Arial, sans-serif",
                axisBorder: {
                    show: true,
                    color: colors.Fg,
                },
                labels: {
                    show: true,
                },
                forceNiceScale: true
            },
        
            legend: {
                show: true,
                fontFamily: "Noto Sans, Arial, sans-serif",
                fontSize: "14px",
            },
            tooltip: {
                enabled: false,
            },
            noData: {
                text: 'Loading...'
            },
        
            fill: {
                type: "solid"
            },
        
            series: [{
                name: 'Sent',
                data: [0],
            }, {
                name: 'Received',
                data: [1],
            }],
            title: {
                text: 'Network Usage',
                align: "left",
                style: {
                    fontFamily: "Noto Sans, Arial, sans-serif",
                    fontSize: "16px",
                    fontWeight: 700,
                }
            },
        }
    }
}

export function netConverter(valBytes, point=0) {
    var result;

    if (valBytes >= 1099511627776)
        result = `${(valBytes/1099511627776).toFixed(point)}TiB`
    else if (valBytes >= 1073741824)
        result = `${(valBytes/1073741824).toFixed(point)}GiB`
    else if (valBytes >= 1048576)
        result = `${(valBytes/1048576).toFixed(point)}MiB`
    else
        result = `${(valBytes/1024).toFixed(point)}KiB`

    return result;
}