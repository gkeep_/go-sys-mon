import { Colors, netConverter, Options } from './options.js';

if (sessionStorage.getItem("isLoggedIn") != "true")
    window.location.href = "/login";

var dataStruct;
var colors = new Colors();
var options = new Options();

const disksTable = document.querySelector("#disksTable");
const tempsTable = document.querySelector("#tempsTable");

window.Apex = {
    fontFamily: "Noto Sans, Arial, sans-serif",
};

//get data from json
fetchData()
setInterval(fetchData, 1000)

function fetchData() {
    $.getJSON({
        url: '/api',
        success: function(response) {
            updateData(response)
        }
    })
}

const memoryWidget = new ApexCharts(document.querySelector("#ramchart"), options.RAM);
const CPUWidget = new ApexCharts(document.querySelector("#cpuchart"), options.CPU);
const networkWidget = new ApexCharts(document.querySelector("#networkChart"), options.Network);

memoryWidget.render();
networkWidget.render();

memoryWidget.updateOptions({
    legend: {
        formatter: function(val) {
            try {
                if (val == "Swap")
                    var memType = dataStruct.Memory.Swap
                else
                    var memType = dataStruct.Memory

                var used = `${(memType.Used/1024).toFixed(1)}GiB/${(memType.Total/1024).toFixed(1)}GiB`
                return `${val}: ${used}`;
            }
            catch (err) {
                if (err instanceof TypeError)
                    console.log("Data not loaded...")
            }
        }
    }
});

function removeAllChildNodes(node){
    while (node.firstChild)
        node.removeChild(node.firstChild);
}

// update charts using new data
function updateData(data) {
    dataStruct = data;

    initCharts(dataStruct);

    appendMemoryData(dataStruct);
    appendCPUData(dataStruct);

    appendTempsData(dataStruct);
    appendDiskData(dataStruct);
    appendNetworkData(dataStruct);
}

function initCharts(dataStruct) {
    try {
        // CPU chart
        // this *should* occur only once since CPUWidget.series is empty
        if (CPUWidget.w.config.series.length < dataStruct.CPU.CoreCount)
        {
            for (var i = 0; i < dataStruct.CPU.CoreCount; i++) {
                CPUWidget.appendSeries({
                    name: `CPU${i}`,
                    data: []
                })
            }
            CPUWidget.updateOptions({
                colors: colors.getColorList(dataStruct.CPU.CoreCount),
            })
            setAdaptiveDesign(windowResizeQueryList);
            CPUWidget.render();
        }
    }
    catch (err) {
        if (err instanceof TypeError)
            console.log("Data not loaded...")
    }
}

function appendCPUData(dataStruct) {
    var load = []
    for (var i = 0; i < dataStruct.CPU.CoreCount; i++) {
        load.push({
            data: [Math.floor(dataStruct.CPU.Load.Percent.PerCore[i])]
        })
    }
    CPUWidget.appendData(load);
    
    CPUWidget.updateOptions({
        legend: {
            formatter: function(val) {
                var used = dataStruct.CPU.Load.Percent.PerCore[parseInt(val.substring(3))].toFixed(0)
                return `${val}: ${used}%`;
            }
        }
    });
}

function appendMemoryData(dataStruct) {
    // Math.round((num + Number.EPSILON) * 100) / 100
    var ramPercentage = Math.round((
        ((dataStruct.Memory.Used / dataStruct.Memory.Total) * 100) + Number.EPSILON) * 100) / 100
    var swapPercentage = Math.round((
        ((dataStruct.Memory.Swap.Used / dataStruct.Memory.Swap.Total) * 100) + Number.EPSILON) * 100) / 100

    memoryWidget.updateOptions({
        series: [Math.floor(ramPercentage), Math.floor(swapPercentage)]
    })
}

function appendDiskData(dataStruct) {
    // clear disksTable children if they exist
    if (disksTable.childElementCount != 0) {
        removeAllChildNodes(disksTable);
        $("#disksTable").append(`
            <thead>
                <th>Disk</th>
                <th>Mountpoint</th>
                <th>Used</th>
                <th>Size</th>
            </thead>`)
    }

    // fill disksTable with data
    for (var i = 0; i < dataStruct.Disks.length; i++) {
        var disk = dataStruct.Disks[i].Device
        var mountpoint = dataStruct.Disks[i].Mountpoint
        var used = Math.round(dataStruct.Disks[i].UsedPercent)
        var size;

        if (dataStruct.Disks[i].TotalSize >= 1000000)
            size = `${(dataStruct.Disks[i].TotalSize / 1000000).toFixed(1)} TB`
        else if (dataStruct.Disks[i].TotalSize >= 1000)
            size = `${(dataStruct.Disks[i].TotalSize / 1000).toFixed(1)} GB`
        else
            size = `${(dataStruct.Disks[i].TotalSize).toFixed(1)} MB`

        $("#disksTable").append(`
        <tr>
            <td>${disk}</td>
            <td>${mountpoint}</td>
            <td>${used}%</td>
            <td>${size}</td>
        </tr>`)
    }
}

function appendTempsData(dataStruct) {
    // clear tempsTable children if they exist
    if (tempsTable.childElementCount != 0) {
        removeAllChildNodes(tempsTable);
        $("#tempsTable").append(`
        <thead>
            <th>Sensor</th>
            <th>Temperature</th>
        </thead>`)
    }

    // fill tempsTable with data
    for (var i = 0; i < dataStruct.Host.Temperatures.length; i++) {
        var name = dataStruct.Host.Temperatures[i].SensorName
        var temp = parseFloat((dataStruct.Host.Temperatures[i].Temperature).toFixed(1))
        var tempHigh = 60
        var tempCritical = 75
        var color = colors.Green

        if (dataStruct.Host.Temperatures[i].ThresholdHigh != 0)
            tempHigh = (dataStruct.Host.Temperatures[i].ThresholdHigh).toFixed(1)

        if (dataStruct.Host.Temperatures[i].ThresholdCritical != 0)
            tempCritical = (dataStruct.Host.Temperatures[i].ThresholdCritical).toFixed(1)

        if (temp >= tempCritical)
            color = colors.Red
        else if (temp >= tempHigh)
            color = colors.Orange

        $("#tempsTable").append(`
        <tr>
            <td>${name}</td>
            <td style="color: ${color}">${temp} C°</td>
        </tr>`)
    }
}

function appendNetworkData(dataStruct) {
    networkWidget.appendData([{
        data: [dataStruct.Host.Network.BytesSent]
    }, {
        data: [dataStruct.Host.Network.BytesRecv]
    }])

    networkWidget.updateOptions({
        legend: {
            formatter: function(val) {
                var used, total;
                if (val == 'Received') {
                    used = dataStruct.Host.Network.BytesRecv
                    total = dataStruct.Host.Network.Total.BytesRecv
                } else {
                    used = dataStruct.Host.Network.BytesSent
                    total = dataStruct.Host.Network.Total.BytesSent
                }

                return `${val}: ${netConverter(used)}/s (Total: ${netConverter(total)})`;
            }
        },
        yaxis: {
            labels: {
                formatter: function(val) {
                    return (`${netConverter(val, 1)}/s`);
                },
            }
        }
    });
}

const colorSchemeQueryList = window.matchMedia('(prefers-color-scheme: dark)');
const setColorScheme = _ => {
    colors.Bg     = getComputedStyle(document.body).getPropertyValue('--color-background');
    colors.Fg     = getComputedStyle(document.body).getPropertyValue('--color-foreground');
    colors.FgAlt  = getComputedStyle(document.body).getPropertyValue('--color-foreground-alt');
    colors.Grid   = getComputedStyle(document.body).getPropertyValue('--color-grid');
    colors.Blue   = getComputedStyle(document.body).getPropertyValue('--color-blue');
    colors.Orange = getComputedStyle(document.body).getPropertyValue('--color-orange');
    colors.Purple = getComputedStyle(document.body).getPropertyValue('--color-purple');
    colors.Green  = getComputedStyle(document.body).getPropertyValue('--color-green');
    colors.Red    = getComputedStyle(document.body).getPropertyValue('--color-red');

    CPUWidget.updateOptions({
        xaxis: {
            axisBorder: {
                color: colors.Fg,
            },
            axisTicks: {
                color: colors.Fg,
            },
            labels: {
                show: true,
                formatter: function(val) {
                    return (val+"s");
                },
            }
        },
        yaxis: {
            max: 100,
            min: 0,
            axisBorder: {
                color: colors.Grid,
            },
            axisTicks: {
                color: colors.Grid,
            },
            labels: {
                show: true,
                formatter: function(val) {
                    return (val+"%");
                },
            },
        },
        grid: {
            borderColor: colors.Grid,
        },
        legend: {
            labels: {
                colors: colors.Fg,
            },
        }
    })

    networkWidget.updateOptions({
        colors: [colors.Orange, colors.Purple],
        xaxis: {
            axisBorder: {
                color: colors.Fg,
            },
            axisTicks: {
                color: colors.Fg,
            },
            labels: {
                show: true,
                formatter: function(val) {
                    return (val+"s");
                },
            },
        },
        yaxis: {
            axisBorder: {
                color: colors.Grid,
            },
            axisTicks: {
                color: colors.Grid,
            },
            labels: {
                show: true,
                formatter: function(val) {
                    return (val.toFixed(0)+"MiB/s");
                },
            },
        },
        grid: {
            borderColor: colors.Grid,
        },
        legend: {
            labels: {
                colors: [colors.Fg]
            },
        },
    })

    memoryWidget.updateOptions({
        colors: [colors.Orange, colors.Blue],
        legend: {
            labels: {
                colors: [colors.Fg],
            }
        }
    })

    window.Apex.colors = [colors.Orange, colors.Blue]
}

setColorScheme(colorSchemeQueryList);
colorSchemeQueryList.addEventListener('change', setColorScheme);

const windowResizeQueryList = window.matchMedia('(max-width: 1000px)');
const setAdaptiveDesign = e => {
    var fontSize, range, position;
    if (e.matches) {
        fontSize = "11px";
        range = 15;
        position = "bottom";
    } else {
        fontSize = "14px";
        range = 30;
        position = "right";
    }
    
    CPUWidget.updateOptions({
        legend: {
            position: position,
            fontSize: fontSize,
        },
        xaxis: {
            range: range
        }
    })
    memoryWidget.updateOptions({
        legend: {
            position: position,
        }
    })
    networkWidget.updateOptions({
        legend: {
            fontSize: fontSize,
        },
        xaxis: {
            range: range
        }
    })
}
windowResizeQueryList.addEventListener('change', setAdaptiveDesign);
