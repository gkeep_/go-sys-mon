package cfg

import (
	"embed"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/yaml.v3"
)

//go:embed config.yaml
var defaultConfig embed.FS

var config *ConfStruct

type ConfStruct struct {
	Port     int
	Username string
	Password string
}

func readConfig() *ConfStruct {
	cfgDir, err := os.UserConfigDir()
	if err != nil {
		fmt.Println(err)
	}
	cfgLocation := fmt.Sprintf("%s/go-sys-mon/config.yaml", cfgDir)
	var data []byte
	var readErr error

	if _, err := os.Stat(cfgLocation); errors.Is(err, os.ErrNotExist) {
		log.Println("Global config not found, using default config...")
		data, readErr = defaultConfig.ReadFile("config.yaml")
	} else {
		data, readErr = ioutil.ReadFile(cfgLocation)
	}
	if readErr != nil {
		log.Fatalf("Error reading config.yaml: [%s]", err)
	}

	config := &ConfStruct{}
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		log.Fatalf("Error parsing config.yaml: [%s]", err)
	}

	config.Password = hashAndSalt(config.Password)
	return config
}

func hashAndSalt(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}

	return string(hash)
}

func GetConfig() ConfStruct {
	if config == nil {
		config = readConfig()
	}
	return *config
}
